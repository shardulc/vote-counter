## VoteCounter: a Java election system with an ever-growing feature set

# What is it?
VoteCounter is a Java application for conducting online elections. It is meant
to be easy to use and has a comprehensive, ever-growing list of features,
including:
- *Support for group-based elections:* For example, in a school with student
  houses, only members of a house should be allowed to vote for the house
  leaders while all students can vote for the treasurer.
- *Multiple parallel clients:* Many clients can run in parallel, being connected
  over the Internet to the same server, so that the election process is quick
  and easy to manage. Setting up the server and clients is hassle-free.
- *XML-based configuration:* VoteCounter reads in its input from an XML file
  containing the data about posts and nominees. This makes changing and sharing
  configuration settings convenient.
- *Robust servers:* VoteCounter's server is designed to accept client failures
  without failing altogether, ensuring that votes already recorded are kept safe
  and other clients are not affected.
- *Customizability:* The client's graphical interface can accomodate a name and
  logo if wanted; nominees can have their own symbols associated with their
  names on screen too.

# Using VoteCounter
See the file named 'HELP' for help and instructions on how to use VoteCounter
and a description of the files in the subdirectories.

# FOSS: Free and Open-Source Software
VoteCounter is licensed under the GPLv3. This means that it gives you the four
basic freedoms of software: the freedom to run the program as you wish, for any
purpose; the freedom to study how the program works, and change it so it does
your computing as you wish (this implies access to the source code); the freedom
to redistribute copies; and the freedom to distribute copies of your modified
versions (this also includes access to the source code).

Since the GPLv3 is a copyleft license, it also implies that anyone
redistributing this software must provide the same terms of distribution. This
mode of copyright not only guarantees your freedom, but also the freedom of
anybody you distribute this software to.

The license itself is available in the 'LICENSE' file in the top directory and
each of the subdirectories (for VoteClient, VoteCommon, and VoteServer). You can
also view the license by clicking on 'License' in the 'File' menu in a running
instance of VoteClient.

# Who's behind this?
I am Shardul Chiplunkar, a high-schooler who has spent the equivalent of
approximately three months working on VoteCounter (in reality, work progressed
mostly during the summer for two years) and is still working on it. You can
contact me at `<firstname>.<lastname>@gmail.com` substituting the angle
brackets with my name.

# Contribute!
Bugs, questions, suggestions, improvements? Feel free to contact me at the
address above or submit a pull request.
