/**
 * VoteServer.java: voting server
 * Copyright (C) 2012 - 2016 Shardul C.
 *
 * This file is part of VoteCounter.
 *
 * VoteCounter is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * VoteCounter is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * VoteCounter. If not, see <http://www.gnu.org/licenses/>.
 *
 * Bugs, tips, suggestions, requests to <shardul.chiplunkar@gmail.com>.
 */
package voteserver;

import com.beust.jcommander.Parameter;
import java.io.*;
import java.util.List;
import javax.swing.*;

/**
 * Server for vote-counting application.
 *
 * {@code VoteServer} is a forking server which can handle multiple clients and
 * record votes in a thread-safe manner. The actual data (some of which is
 * passed to the client) is read and parsed from an input XML file by
 * {@link InputParser}.
 *
 * {@code VoteServer} can exit with the following exit codes:
 * <ul>
 *   <li><code>0</code> or none: normal exit</li>
 *   <li><code>1</code>: backup-related error</li>
 *   <li><code>2</code>: error reading or parsing input file</li>
 *   <li><code>3</code>: error in graphical input dialogs</li>
 *   <li><code>4</code>: output-related error</li>
 *   <li><code>5</code>: error in client connection or communication</li>
 * </ul>
 *
 * @author Shardul C.
 */
public class VoteServer {

    private static int votes[][]; // stores number of votes as votes[post][nominee]
    private static int voters[]; // stores number of voters as {total, group 1, group 2, ...}
    private static String inPath; // input XML file
    private static String outPath; // output results file
    private static String backupPath; // backup file
    private static final int ROW_WIDTH = 25; // spacing width in results file

    // for command-line arguments
    private static com.beust.jcommander.JCommander jc;
    static CommandLineArgs cla;

    // information about groups, posts, and nominees
    static List<String> groups;
    static List<String> genericPosts;
    static List<List<String>> genericNominees;
    static List<String> nonGenericPosts;
    static List<List<List<String>>> nonGenericNominees;
    static List<String> allPosts; // 'flat' posts list, used for indexing

    /**
     * The main method.
     *
     * This method gets the input and output paths from {@link
     * #getInOutPaths(java.util.List)} and reads the input file with {@link
     * voteserver.InputParser}.
     *
     * Following that, this method enters a never-ending loop to listen for
     * client connections. If no arguments were given, a 'Running...' dialog is
     * also shown.
     *
     * @param args try '--help'
     */
    public static void main(String[] args) {
        System.out.println("VoteCounter Copyright (C) 2012 - 2016 Shardul C.");
        System.out.println("This program comes with ABSOLUTELY NO WARRANTY. " +
                "This is free software, and you are welcome to redistribute " +
                "it under certain conditions; see the LICENSE file for more " +
                "details.");

        cla = new CommandLineArgs();
        if (args.length == 0) {
            getInOutPaths(new java.util.ArrayList<String>());

            JOptionPane.showMessageDialog(null, "Please choose the logo...",
                    "Choose logo", JOptionPane.PLAIN_MESSAGE);
            JFileChooser jfc = new JFileChooser();
            jfc.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("Image files",
                    "png", "jpg", "jpeg", "gif"));
            int opt = jfc.showOpenDialog(null);
            if (opt == JFileChooser.APPROVE_OPTION) {
                cla.orgLogo = jfc.getSelectedFile().getPath();
            }

            String inp = JOptionPane.showInputDialog(null, "Enter the name of your organization",
                    "Organization Name", JOptionPane.QUESTION_MESSAGE);
            if (inp != null && inp.length() > 0) {
                cla.orgName = inp;
            }
        } else {
            jc = new com.beust.jcommander.JCommander(cla, args); // parse args
            jc.setProgramName("VoteServer");
            if (cla.help) {
                jc.usage();
                System.exit(0);
            }
            getInOutPaths(cla.files);
        }

        try (java.net.ServerSocket sock = new java.net.ServerSocket(Integer.parseInt(messages.Messages.PORT.msg))) {
            voteserver.InputParser parser = new voteserver.InputParser();
            parser.parse(new FileInputStream(inPath));
            groups = parser.getGroups();
            genericPosts = parser.getGenericPosts();
            genericNominees = parser.getGenericNominees();
            nonGenericPosts = parser.getNonGenericPosts();
            nonGenericNominees = parser.getNonGenericNominees();

            if (cla.backup != null && cla.backup.length() > 0) {
                try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cla.backup))) {
                    backupPath = cla.backup;
                    if (!ois.readUTF().equals(inPath)) {
                        System.err.println("Provided input file and backup do not match!");
                        System.exit(1);
                    }
                    votes = (int[][]) ois.readObject();
                    voters = (int[]) ois.readObject();
                } catch (IOException | ClassNotFoundException ex) {
                    System.err.println("Error reading backup file: " + ex.getLocalizedMessage());
                    System.exit(1);
                }
            } else {
                backupPath = "VoteServer" + System.currentTimeMillis() + ".vsb"; // default backup file name
                initVoteArrays();
            }

            allPosts = new java.util.ArrayList<>();
            allPosts.addAll(genericPosts);
            // flatten nongeneric posts
            for (String post: nonGenericPosts) {
                for (String group: groups) {
                    allPosts.add(group + " " + post);
                }
            }

            if (args.length == 0) {
                //<editor-fold defaultstate="collapsed" desc="Set 'Nimbus L&F (optional)">
                /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
                 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
                 */
                try {
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
                    System.err.println("Caught exception in 'look and feel' code: " + ex.getLocalizedMessage());
                }
                //</editor-fold>
                showDialog();
            }
            System.out.println("server started");
            while (true) {
                new voteserver.VoteServerThread(sock.accept()).start(); // blocking accept call
                System.out.println("client started");
            }
        } catch (IOException | org.jdom2.JDOMException ex) {
            System.err.println("Error parsing input file: " + ex.getLocalizedMessage());
            System.exit(2);
        }
    }

    /**
     * Gets input and output paths.
     *
     * This method checks whether any arguments are provided and if they are in
     * the correct format. If they are valid, then the thereby given input and
     * output files are used; if they are not valid, a usage message is printed
     * and the server exits. If no arguments are given then a graphical file
     * selector and input box is provided for the input file and output file,
     * respectively.
     *
     * @param args the command-line arguments
     */
    private static void getInOutPaths(List<String> args) {
        if (args.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please choose an input XML file...",
                    "Choose input file", JOptionPane.PLAIN_MESSAGE);
            JFileChooser jfc = new JFileChooser();
            jfc.setFileFilter(new javax.swing.filechooser.FileNameExtensionFilter("XML files", "xml"));
            int opt = jfc.showOpenDialog(null);
            if (opt == JFileChooser.CANCEL_OPTION) {
                JOptionPane.showMessageDialog(null, "No input file chosen, exiting application.",
                        "No Input File Chosen", JOptionPane.WARNING_MESSAGE);
                System.exit(3);
            } else if (opt == JFileChooser.ERROR_OPTION) {
                System.err.println("Error in JFileChooser!");
                System.exit(3);
            }
            inPath = jfc.getSelectedFile().getPath();

            outPath = JOptionPane.showInputDialog("Enter the output file name:", "results.txt");
            if (outPath.isEmpty()) {
                JOptionPane.showMessageDialog(null, "No output file chosen, exiting application.",
                        "No Output File Chosen", JOptionPane.WARNING_MESSAGE);
                System.exit(3);
            }
        } else if (args.size() == 2) {
            if (!((new File(args.get(0))).exists() && args.get(0).endsWith("xml"))) {
                jc.usage();
                System.exit(2);
            }
            inPath = args.get(0);
            outPath = args.get(1);
        } else {
            jc.usage();
            System.exit(0);
        }
    }

    /**
     * Initializes arrays {@code votes} and {@code voters}.
     */
    private static void initVoteArrays() {
        votes = new int[genericPosts.size() + nonGenericPosts.size() * groups.size()][];
        for (int i = 0; i < genericPosts.size(); i++) {
            votes[i] = new int[genericNominees.get(i).size()];
        }
        for (int i = 0; i < nonGenericPosts.size(); i++) {
            for (int j = 0; j < groups.size(); j++) {
                votes[genericPosts.size() + i * groups.size() + j] = new int[nonGenericNominees.get(i).get(j).size()];
            }
        }
        voters = new int[groups.size() + 1];
    }

    /**
     * Shows a 'Server running...' dialog.
     *
     * The dialog also has a 'Stop server' button which confirms the action and
     * immediately stops the server and the voting process.
     */
    private static void showDialog() {
        final JFrame serverFrame = new JFrame("Server Running");
        serverFrame.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        JPanel serverPanel = new JPanel(new java.awt.BorderLayout(5, 5));
        serverPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        serverPanel.add(new JLabel("Server running...", SwingConstants.CENTER), java.awt.BorderLayout.CENTER);
        JButton stop = new JButton("Stop server");
        stop.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                if (JOptionPane.showConfirmDialog(serverFrame, "Are you sure you want to stop the server? The voting process will be stopped.",
                        "Stop Server", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    System.out.println("server terminated");
                    writeVotes();
                    System.exit(0);
                }
            }
        });
        serverPanel.add(stop, java.awt.BorderLayout.SOUTH);
        serverPanel.setPreferredSize(new java.awt.Dimension(280, 70));

        serverFrame.add(serverPanel);
        serverFrame.pack();
        serverFrame.setVisible(true);
    }

    /**
     * Writes votes to output file and updates backup file.
     */
    synchronized static void writeVotes() {
        // write results
        try (BufferedWriter bw = java.nio.file.Files.newBufferedWriter(java.nio.file.Paths.get(outPath),
                java.nio.charset.StandardCharsets.UTF_8, java.nio.file.StandardOpenOption.WRITE,
                java.nio.file.StandardOpenOption.CREATE, java.nio.file.StandardOpenOption.TRUNCATE_EXISTING)) {
            bw.write("Total number of voters: " + voters[0]);
            bw.newLine();
            for (int i = 0; i < groups.size(); i++) {
                bw.write(groups.get(i) + " voters: " + voters[i+1]);
                bw.newLine();
            }

            // write generic results
            bw.newLine();
            bw.write("Generic posts:");
            bw.newLine();
            List<String> gen = allPosts.subList(0, genericPosts.size());
            for (int i = 0; i < gen.size(); i++) {
                bw.write(genericPosts.get(i) + " --");
                bw.newLine();
                List<String> noms = genericNominees.get(i);
                for (int j = 0; j < noms.size(); j++) {
                    bw.write(noms.get(j));
                    for(int k = 0; k < (ROW_WIDTH - noms.get(j).length()); k++) {
                        bw.write(" ");
                    }
                    bw.write(Integer.toString(votes[i][j]));
                    bw.newLine();
                }
                bw.newLine();
            }

            // write nongeneric results
            if (nonGenericPosts.size() > 0) {
                bw.newLine();
                bw.newLine();
                bw.write("Nongeneric posts:");
                bw.newLine();

                List<String> nonGen = allPosts.subList(genericPosts.size(), allPosts.size());
                for (int i = 0; i < nonGen.size(); i++) {
                    List<String> noms = nonGenericNominees.get(i / groups.size()).get(i % groups.size());
                    if (noms.size() > 0) {
                        bw.write(nonGen.get(i) + " --");
                        bw.newLine();
                    }
                    for (int j = 0; j < noms.size(); j++) {
                        bw.write(noms.get(j));
                        for (int k = 0; k < ROW_WIDTH - noms.get(j).length(); k++) {
                            bw.write(" ");
                        }
                        bw.write(Integer.toString(votes[genericPosts.size() + i][j]));
                        bw.newLine();
                    }
                    bw.newLine();
                }
            }
        } catch (IOException ex) {
            System.err.println("Error writing output file: " + ex.getLocalizedMessage());
            System.exit(4);
        }

        // write backup
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(backupPath))) {
            oos.writeUTF(inPath);
            oos.writeObject(votes);
            oos.writeObject(voters);
        } catch (IOException ex) {
            System.err.println("Error writing backup file: " + ex.getLocalizedMessage());
            System.exit(1);
        }
    }

    /**
     * Increments specified vote category.
     *
     * @param i post
     * @param j nominee
     */
    synchronized static void incVotes(int i, int j) {
        votes[i][j]++;
    }

    /**
     * Increments specified voter count.
     *
     * @param i index of {@code voters} to be incremented
     */
    synchronized static void incVoters(int i) {
        voters[i]++;
    }

    /**
     * Class to hold command-line arguments via {@code JCommander}.
     *
     * For a description of the arguments, try running VoteServer with the
     * '--help' argument.
     */
    static class CommandLineArgs {
        @Parameter(description = "<input.xml> <output.txt>")
        List<String> files;
        @Parameter(names = {"-n", "--org-name"}, description = "organization name")
        String orgName;
        @Parameter(names = {"-l", "--org-logo"}, description = "path to organization logo")
        String orgLogo;
        @Parameter(names = {"-b", "--backup"}, description = "path to backup file")
        String backup;
        @Parameter(names = {"-h", "--help", "--usage"}, description = "show help")
        boolean help = false;
    }
}
