/**
 * InputParser.java: parses XML input
 * Copyright (C) 2012 - 2014, 2016 Shardul C.
 *
 * This file is part of VoteCounter.
 *
 * VoteCounter is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * VoteCounter is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * VoteCounter. If not, see <http://www.gnu.org/licenses/>.
 *
 * Bugs, tips, suggestions, requests to <shardul.chiplunkar@gmail.com>.
 */
package voteserver;

import java.util.ArrayList;
import java.util.List;
import org.jdom2.Element;
import org.jdom2.JDOMException;

/**
 * XML input parser for VoteCounter.
 *
 * This class provides methods for parsing an XML input file and converting the
 * data into a format usable by VoteCounter.
 *
 * A sample XML input file named {@code demo.xml} is provided in the VoteServer
 * directory to illustrate the required format.
 *
 * Of course, there can be no groups (or equivalently, a missing {@code
 * <groups>} tag) in which case there cannot be any nongeneric posts. There can
 * be as many groups and posts as desired, with any number of nominees and
 * groups contesting each post.
 *
 * @author shardul
 */
public class InputParser {

    // data to be parsed
    private final List<String> groups = new ArrayList<>();
    private final List<String> genericPosts = new ArrayList<>();
    private final List<List<String>> genericNominees = new ArrayList<>();
    private final List<String> nonGenericPosts = new ArrayList<>();
    private final List<List<List<String>>> nonGenericNominees = new ArrayList<>();

    /**
     * Parse the XML input file and get groups, posts, and nominees.
     *
     * The {@code parse} method parses the document using
     * {@code org.jdom2.input.SAXBuilder} to traverse the XML 'tree',
     * interpreting the tags and data as required.
     *
     * @param is an {@link java.io.InputStream} for the XML input file
     * @throws JDOMException if XML structure/syntax is incorrect
     * @throws java.io.IOException if error accessing file
     */
    public void parse(java.io.InputStream is) throws JDOMException, java.io.IOException {
        Element root = ((new org.jdom2.input.SAXBuilder()).build(is)).getRootElement();

        Element child = root.getChild("groups");
        if (child != null) {
            List<Element> groupList = child.getChildren();
            for (int i = 0; i < groupList.size(); i++) {
                groups.add(groupList.get(i).getText());
            }
        }

        child = root.getChild("posts").getChild("generic");
        if (child != null) {
            List<Element> genericPostElements = child.getChildren();
            for (int i = 0; i < genericPostElements.size(); i++) {
                String curPostName = genericPostElements.get(i).getAttributeValue("name");
                if (curPostName == null) {
                    throw new JDOMException("Unnamed generic post!");
                }
                genericPosts.add(curPostName);
                genericNominees.add(new ArrayList<String>());
                List<Element> currentNominees = genericPostElements.get(i).getChildren();
                if (currentNominees.isEmpty()) {
                    throw new JDOMException("No nominees for generic post " + curPostName + "!");
                }
                for (int j = 0; j < currentNominees.size(); j++) {
                    genericNominees.get(i).add(currentNominees.get(j).getText());
                }
            }
        }

        child = root.getChild("posts").getChild("nongeneric");
        if (child != null) {
            List<Element> nonGenericPostElements = child.getChildren();
            for (int i = 0; i < nonGenericPostElements.size(); i++) {
                String curPostName = nonGenericPostElements.get(i).getAttributeValue("name");
                if (curPostName == null) {
                    throw new JDOMException("Unnamed nongeneric post!");
                }
                nonGenericPosts.add(curPostName);
                List<Element> currentGroups = nonGenericPostElements.get(i).getChildren();
                if (currentGroups.isEmpty()) {
                    throw new JDOMException("No groups for nongeneric post " + curPostName + "!");
                }
                nonGenericNominees.add(new ArrayList<List<String>>());
                for (int k = 0; k < groups.size(); k++) {
                    nonGenericNominees.get(i).add(new ArrayList<String>());
                }
                for (int j = 0; j < currentGroups.size(); j++) {
                    List<Element> currentNominees = currentGroups.get(j).getChildren();
                    String curGroupName = currentGroups.get(j).getAttributeValue("name");
                    if (curPostName == null) {
                        throw new JDOMException("Unnamed group in nongeneric post " + curPostName + "!");
                    }
                    if (currentNominees.isEmpty()) {
                        throw new JDOMException("No nominees for nongeneric post " + curPostName
                                + " for group " + curGroupName + "!");
                    }
                    int curGroupIndex = groups.indexOf(curGroupName);
                    if (curGroupIndex == -1) {
                        throw new JDOMException("Unknown group for nongeneric post " + curPostName + "!");
                    }
                    for (int k = 0; k < currentNominees.size(); k++) {
                        nonGenericNominees.get(i).get(curGroupIndex).add(currentNominees.get(k).getText());
                    }
                }
            }
        }
    }

    /**
     * Get nominees for generic posts.
     *
     * The return value is a two-dimensional list, with the first dimension
     * representing posts and the second, individual nominees.
     *
     * @return nominees for generic posts
     */
    public List<List<String>> getGenericNominees() {
        return genericNominees;
    }

    /**
     * Get nominees for non-generic posts.
     *
     * The return value is a three-dimensional list, with the first dimension
     * representing posts, the second, groups, and the third, individual
     * nominees.
     *
     * @return nominees for non-generic posts
     */
    public List<List<List<String>>> getNonGenericNominees() {
        return nonGenericNominees;
    }

    /**
     * Get list of nominee groups.
     *
     * @return list of nominee groups
     */
    public List<String> getGroups() {
        return groups;
    }

    /**
     * Get list of generic (non-group) posts.
     *
     * @return list of generic posts
     */
    public List<String> getGenericPosts() {
        return genericPosts;
    }

    /**
     * Get list of non-generic (group-wise) posts.
     *
     * @return list of non-generic posts
     */
    public List<String> getNonGenericPosts() {
        return nonGenericPosts;
    }
}
