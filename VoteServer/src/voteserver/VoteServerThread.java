/**
 * VoteServerThread.java: voting server thread
 * Copyright (C) 2012 - 2016 Shardul C.
 *
 * This file is part of VoteCounter.
 *
 * VoteCounter is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * VoteCounter is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * VoteCounter. If not, see <http://www.gnu.org/licenses/>.
 *
 * Bugs, tips, suggestions, requests to <shardul.chiplunkar@gmail.com>.
 */
package voteserver;

import static voteserver.VoteServer.*;

/**
 * Server thread to handle a single client.
 *
 * {@code VoteServerThread} is created and started by {@link VoteServer} when it
 * accepts a client connection. {@code VoteServerThread} then creates I/O
 * streams, sends required data, and begins receiving and recording votes from
 * the client until the client terminates.
 *
 * @author Shardul C.
 */
public class VoteServerThread extends Thread {

    // client communication streams
    private java.io.ObjectInputStream in;
    private java.io.ObjectOutputStream out;

    private final boolean[][] votes;

    /**
     * Constructs a new {@code VoteServerThread} connected to the client.
     *
     * The client's connection has already been accepted and a corresponding
     * {@link java.net.Socket} is passed to the thread. The I/O streams function
     * through this {@link java.net.Socket}. The required data -- post and
     * nominee information -- is sent to the client, and the method loops,
     * receiving and recording votes from the client.
     *
     * @param sock a {@link java.net.Socket} to the client
     */
    public VoteServerThread(java.net.Socket sock) {
        super("VoteServerThread");

        try {
            // create i/o streams
            this.out = new java.io.ObjectOutputStream(sock.getOutputStream());
            out.flush(); // required, otherwise client and server wait for each other
            this.in = new java.io.ObjectInputStream(sock.getInputStream());
        } catch (java.io.IOException ex) {
            System.err.println("Error connecting to client: " + ex.getLocalizedMessage());
            System.exit(5);
        }

        votes = new boolean[genericPosts.size() + nonGenericPosts.size() * groups.size()][];
        for (int i = 0; i < genericPosts.size(); i++) {
            votes[i] = new boolean[genericNominees.get(i).size()];
        }
        for (int i = 0; i < nonGenericPosts.size(); i++) {
            for (int j = 0; j < groups.size(); j++) {
                votes[genericPosts.size() + i * groups.size() + j] = new boolean[nonGenericNominees.get(i).get(j).size()];
            }
        }
    }

    @Override
    public void run() {
        try {
            // send data
            out.writeObject(groups);
            out.writeObject(genericPosts);
            out.writeObject(genericNominees);
            out.writeObject(nonGenericPosts);
            out.writeObject(nonGenericNominees);
            out.writeObject(cla.orgName);
            if (cla.orgLogo != null && (new java.io.File(cla.orgLogo)).exists()) {
                out.writeObject(new javax.swing.ImageIcon(cla.orgLogo));
            } else {
                out.writeObject(null);
            }

            String parts[]; // parts after splitting input from client
            String group; // current voter's group
            boolean done = false; // loop exit

            while (true) {
                out.reset();
                // what group?
                out.writeObject(messages.Messages.GET_GROUP);
                group = in.readObject().toString();

                // client terminated
                if (group.equals(messages.Messages.GOODBYE.toString())) {
                    System.out.println("client terminated");
                    break;
                }

                // get votes
                while (true) {
                    String rec = (String) in.readObject();
                    // client terminated
                    if (rec.equals(messages.Messages.GOODBYE.toString())) {
                        done = true;
                        break;
                    }
                    // current voter over
                    if (rec.equals(messages.Messages.NEXT_VOTER.toString())) {
                        break;
                    }
                    parts = rec.split(";"); // Messages.VOTE ; post name ; nominee number
                    votes[allPosts.indexOf(parts[1])][Integer.parseInt(parts[2])] = true;
                }

                // increment total and group voter numbers
                incVoters(0);
                if (!group.equals(messages.Messages.NO_GROUP.toString())) {
                    incVoters(VoteServer.groups.indexOf(group) + 1);
                }

                // increment actual votes
                for (int i = 0; i < votes.length; i++) {
                    for (int j = 0; j < votes[i].length; j++) {
                        if (votes[i][j]) {
                            votes[i][j] = false;
                            incVotes(i, j);
                        }
                    }
                }
                writeVotes();

                if (((String) in.readObject()).equals(messages.Messages.GOODBYE.toString()) || done) {
                    System.out.println("client terminated");
                    break;
                }
            }
            in.close();
            out.close();
        } catch (java.io.IOException | ClassNotFoundException ex) {
            System.err.println("Error communicating with client: " + ex.getLocalizedMessage());
            System.exit(5);
        }
    }
}
