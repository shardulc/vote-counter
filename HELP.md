# How to Use VoteCounter

There are three subdirectories in this directory:
- VoteClient: the client end of VoteCounter
- VoteCommon: the common library used by the server and the client
- VoteServer: the server end of VoteCounter

On the machine intended to run the server, copy the `VoteServer/dist` directory
in its entirety to a convenient location (or, if you want, you can just work
within the directory). You can run the server with no command-line arguments,
like this:

    java -jar VoteServer.jar

whereupon you will be shown dialog boxes to select the input XML file, output
text file, and organization name and logo. Or, you can run

    java -jar VoteServer.jar --help

to see a summary of all the available arguments: 

    Usage: VoteServer [options] <input.xml> <output.txt>
      Options:
        -b, --backup
           if provided, treat output file as backup input
           Default: false
        -l, --org-logo
           path to organization logo
        -n, --org-name
           organization name
        -h, --help, --usage
           show help
           Default: false 

The two unnamed arguments, `<input.xml>` and `<output.txt>`, should either both
be given, or none be given to cause the graphical file chooser to be shown. If
any command-line arguments are provided, the console alone will indicate that
the server is running; if none are provided, there will be a dialog box to that
effect.

By default, a backup file called `VoteServerXXX.vsb` (where `XXX` is a unique
number, up to Y2K38) is created with backup information whenever VoteServer is
run. In the event of a server crash, VoteServer can be restarted and restored to
the pre-crash state with the `--backup` option; specifically, by passing the
argument `--backup` followed by the backup file name. For a backup to work, the
input file must not have changed between the crash and the restart.

On the machine intended to run the client, copy the `VoteClient/dist` directory
in its entirety to a convenient location (or, if you want, you can just work
within the directory). You can run the client with no command-line arguments,
like this:

    java -jar VoteClient.jar

or with a single argument:

    java -jar VoteClient.jar --no-local

The only difference between the two is that the first automatically checks
whether a server is running locally (i.e. accessible at `127.0.0.1`) and only
prompts the user for the server's IP address if it doesn't find one. The second
prompts for the IP address straightaway. (It should be noted that the server
should be accessible from the client over an IP network.)

The client is capable of displaying symbols for nominees alongside their names.
However, to reduce network usage, this feature is implemented on the
client-side. To assign symbols to names, a directory named `symbols` should be
made in the directory where the client will be run, and image files with nominee
symbols should be placed in it. The image files should be named as follows: for
a nominee called 'Abcde Fghij', the image file would be named `abcdefghij.png`
or `abcdefghij.jpg` (no spaces, no capitals).

The client cannot be cleanly exited during a voter's voting process, to ensure
that the voting isn't left incomplete. To exit the client, an 'Exit' option is
available at the 'next voter' and 'choose group' dialog boxes.

*Bonus!* Each of the three subdirectories is a NetBeans project, so modifying
the code and testing your changes is even easier! The sources are painstakingly
well-documented and well-organized.
